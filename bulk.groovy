
import groovy.json.*
import jenkins.plugins.http_request.HttpRequest

node('master')  {
    stage('git checkout') {
        checkout scmGit(branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[credentialsId: 'test-creds', url: 'git@gitlab.com:ayildiz89/dynamic_id_copy_file_to_another.git']])
    }
    def block
    def blocks
    //def root
    //def newJson
    //def results
    def url
    def process
    def satir

    //parse json data
    stage('ParseJson') {
         url= "https://newposs-jenkins.newposs.gebit.dev/job/till/job/mco-perf-regression/lastSuccessfulBuild/artifact/test-repo-parent/test-parent/integrity-parent/execution/performance-pos/benchmark/performance-results.json"
         sh "curl -o /var/jenkins_home/workspace/performance-results.json ${url}"

        def jsonFilePath = '/var/jenkins_home/workspace/dynamic_id_copy_file_to_another/performance-results.json'

        @NonCPS
          def parseJsonFile = { filePath ->
            def fileContents = readFile file: filePath
            def slurper = new JsonSlurper()
            slurper.parseText(fileContents)
          }
        def json = parseJsonFile(jsonFilePath)

        def branch = json.branch
        def date = json.date
        def tillVersion = json.tillVersion
        blocks = ''

        json.each { id, data ->
            if (id.length() == 2) {
                data.each { ptm, values ->

                    if (values != null) {
                        def verCode = UUID.randomUUID().toString()
                             satir = [
                            "create" :[ "_index" : "myindex" , "_id" : verCode ]
                        ]
                        block = [
                          id : verCode,
                          branch: branch,
                          date: date,
                          tillVersion: tillVersion,
                          suite: ptm,
                          country: values.country,
                          executionTime: values.executionTime,
                          articleCount : values.indicators.articleCount != null ? values.indicators.articleCount : 0,
                          receiptCount : values.indicators.receiptCount != null ? values.indicators.receiptCount : 0,
                          duration:values.processDuration
                          ] 

                    blocks = blocks + JsonOutput.prettyPrint(JsonOutput.toJson(satir)).replaceAll('\\r', '').replaceAll('\\n', ' ') + System.getProperty("line.separator") 
                    blocks = blocks + JsonOutput.prettyPrint(JsonOutput.toJson(block)).replaceAll('\\r', '').replaceAll('\\n', ' ') + System.getProperty("line.separator") 
                }
            println blocks
            }
        }
    }
  ///  println test
   // writeFile  file: '/var/jenkins_home/workspace/dynamic_id_copy_file_to_another/performance-results.json', text: blocks
    httpRequest(
            //consoleLogResponseBody: true,
            contentType: 'APPLICATION_JSON',
            httpMode: 'POST',
            requestBody: blocks,
            //multipartName: '/var/jenkins_home/workspace/dynamic_id_copy_file_to_another/performance-results.json',
            responseHandle: 'NONE',
            url: 'http://192.168.178.150:9200/_bulk?pretty',
            validResponseCodes: '200,404,201'
    ) 
    }
}