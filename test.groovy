import groovy.json.*
import jenkins.plugins.http_request.HttpRequest

node('master')  {
    stage('git checkout') {
        checkout scmGit(branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[credentialsId: 'test-creds', url: 'git@gitlab.com:ayildiz89/dynamic_id_copy_file_to_another.git']])
    }


    //parse json data
    stage('ParseJson') {
       
    

        def jsonFilePath = '/var/jenkins_home/workspace/dynamic_id_copy_file_to_another/test.json'

  // println test
  //  writeFile  file: '/var/jenkins_home/workspace/dynamic_id_copy_file_to_another/performance-results.json', text: test
    httpRequest(
            //consoleLogResponseBody: true,
            contentType: 'APPLICATION_JSON',
            httpMode: 'POST',
            requestBody: jsonFilePath,
            //multipartName: '/var/jenkins_home/workspace/dynamic_id_copy_file_to_another/performance-results.json',
            responseHandle: 'NONE',
            url: 'http://192.168.178.150:9200/regression_json_5/_doc',
            validResponseCodes: '200,404,201'
    ) 
}
}